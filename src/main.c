#include "tests/free_one_block_test.h"
#include "tests/free_two_blocks_test.h"
#include "tests/memory_allocation_test.h"
#include "tests/memory_extension_test.h"
#include "tests/memory_extension_separated_test.h"
int main() {
    void * heap = heap_init(10000);
    first_test_result_writer(memory_allocation_test(heap));
    second_test_result_writer(free_one_block_test(heap));
    third_test_result_writer(free_two_blocks_test(heap));
    fourth_test_result_writer(memory_extension_test(heap));
    fifth_test_result_writer(memory_extension_separated_test(heap));
    return 0;
}
