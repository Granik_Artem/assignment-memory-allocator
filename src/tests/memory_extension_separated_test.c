#include "memory_extension_separated_test.h"

bool memory_extension_separated_test(void* heap){
    void * cell1 = _malloc(9000);
    struct block_header * block1 = (struct block_header*) (((uint8_t*)cell1)-offsetof(struct block_header, contents));
    debug_heap(stderr, heap);
    struct block_header * last =block1;
    while(last->next){
        last = last->next;
    }
    void* buffer_address = (uint8_t *) last + size_from_capacity(last->capacity).bytes;
    map_pages(buffer_address, sysconf(_SC_PAGESIZE)*100, MAP_FIXED);
    debug_heap(stderr, heap);
    void * cell2 = _malloc(20000);
    debug_heap(stderr, heap);
    if(!cell2){
        heap_clean(heap);
        return false;
    }
    struct block_header * block2 = (struct block_header*) (((uint8_t*)cell2)-offsetof(struct block_header, contents));
    if(buffer_address == block2){
        fprintf(stderr, "wrong address int the end\n");
        heap_clean(heap);
        return false;
    }
    _free(cell1);
    _free(cell2);
    debug_heap(stderr, heap);
    heap_clean(heap);
    return true;
}

void fifth_test_result_writer(bool result){
    if(result){
        fprintf(stderr, "Successfully extended memory with a separate region!\n");
    }else{
        fprintf(stderr, "Failed to extend memory with a separate region. \n");
    }
}

