#ifndef MEMORY_ALLOCATOR_FREE_TWO_BLOCKS_TEST_H
#define MEMORY_ALLOCATOR_FREE_TWO_BLOCKS_TEST_H

#endif //MEMORY_ALLOCATOR_FREE_TWO_BLOCKS_TEST_H
#include "../mem.h"
#include "../mem_internals.h"
#include "../util.h"

bool free_two_blocks_test(void* heap);

void third_test_result_writer(bool result);

