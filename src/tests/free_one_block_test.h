#ifndef MEMORY_ALLOCATOR_FREE_ONE_BLOCK_TEST_H
#define MEMORY_ALLOCATOR_FREE_ONE_BLOCK_TEST_H

#endif //MEMORY_ALLOCATOR_FREE_ONE_BLOCK_TEST_H
#include "../mem.h"
#include "../mem_internals.h"
#include "../util.h"

bool free_one_block_test(void* heap);

void second_test_result_writer(bool result);
