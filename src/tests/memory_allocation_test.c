#include "memory_allocation_test.h"

bool memory_allocation_test(void* heap){
    debug_heap(stderr, heap);
    void * cell1 = _malloc(0);
    void * cell2 = _malloc(100);
    void * cell3 = _malloc(110);
    void * cell4 = _malloc(120);
    if(!cell1 || !cell2 || !cell3 || !cell4){
        heap_clean(heap);
        return false;
    }
    debug_heap(stderr, heap);
    struct block_header * block1 = (struct block_header*) (((uint8_t*)cell1)-offsetof(struct block_header, contents));
    struct block_header * block2 = (struct block_header*) (((uint8_t*)cell2)-offsetof(struct block_header, contents));
    struct block_header * block3 = (struct block_header*) (((uint8_t*)cell3)-offsetof(struct block_header, contents));
    struct block_header * block4 = (struct block_header*) (((uint8_t*)cell4)-offsetof(struct block_header, contents));
    if(block1->is_free || block2->is_free || block3->is_free || block4->is_free){
        heap_clean(heap);
        return false;
    }
    debug_heap(stderr, heap);
    if(block1->capacity.bytes != 24 || block2->capacity.bytes != 100 || block3->capacity.bytes != 110 || block4->capacity.bytes != 120){
        heap_clean(heap);
        return false;
    }
    _free(cell1);
    _free(cell2);
    _free(cell3);
    _free(cell4);
    debug_heap(stderr, heap);
    heap_clean(heap);
    return true;
}

void first_test_result_writer(bool result){
    if(result){
        fprintf(stderr, "Successful memory allocation!\n");
    }else{
        fprintf(stderr, "Failed to allocate memory. \n");
    }
}

