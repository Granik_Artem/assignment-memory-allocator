#ifndef MEMORY_ALLOCATOR_MEMORY_ALLOCATION_TEST_H
#define MEMORY_ALLOCATOR_MEMORY_ALLOCATION_TEST_H

#endif //MEMORY_ALLOCATOR_MEMORY_ALLOCATION_TEST_H
#include "../mem.h"
#include "../mem_internals.h"
#include "../util.h"

bool memory_allocation_test(void* heap);

void first_test_result_writer(bool result);
