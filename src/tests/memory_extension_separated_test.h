#ifndef MEMORY_ALLOCATOR_MEMORY_EXTENSION_SEPARATED_TEST_H
#define MEMORY_ALLOCATOR_MEMORY_EXTENSION_SEPARATED_TEST_H

#endif //MEMORY_ALLOCATOR_MEMORY_EXTENSION_SEPARATED_TEST_H
#include "../mem.h"
#include "../mem_internals.h"
#include "../util.h"
#include <sys/mman.h>
#include <unistd.h>
#include <stdlib.h>

bool memory_extension_separated_test(void* heap);

void fifth_test_result_writer(bool result);

