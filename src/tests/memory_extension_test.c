#include "memory_extension_test.h"

bool memory_extension_test(void* heap){
    void * cell1 = _malloc(8000);
    debug_heap(stderr, heap);
    void * cell2 = _malloc(8000);
    debug_heap(stderr, heap);
    if(!cell2){
        heap_clean(heap);
        return false;
    }
    struct block_header * block1 = (struct block_header*) (((uint8_t*)cell1)-offsetof(struct block_header, contents));
    struct block_header * block2 = (struct block_header*) (((uint8_t*)cell2)-offsetof(struct block_header, contents));
    if(block1->next != block2){
        heap_clean(heap);
        return false;
    }
    _free(cell1);
    _free(cell2);
    debug_heap(stderr, heap);
    heap_clean(heap);
    return true;
}

void fourth_test_result_writer(bool result){
    if(result){
        fprintf(stderr, "Successfully extended memory!\n");
    }else{
        fprintf(stderr, "Failed to extend memory. \n");
    }
}

