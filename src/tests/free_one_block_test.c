#include "free_one_block_test.h"


bool free_one_block_test(void* heap){
    debug_heap(stderr, heap);
    void * cell1 = _malloc(100);
    void * cell2 = _malloc(110);
    void * cell3 = _malloc(120);
    debug_heap(stderr, heap);
    struct block_header * block2 = (struct block_header*) (((uint8_t*)cell2)-offsetof(struct block_header, contents));
    _free(cell2);
    debug_heap(stderr, heap);
    if(!block2->is_free){
        heap_clean(heap);
        return false;
    }
    _free(cell1);
    _free(cell3);
    debug_heap(stderr, heap);
    heap_clean(heap);
    return true;
}

void second_test_result_writer(bool result){
    if(result){
        fprintf(stderr, "Successfully freed one block!\n");
    }else{
        fprintf(stderr, "Failed to free one block. \n");
    }
}

