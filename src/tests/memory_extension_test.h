#ifndef MEMORY_ALLOCATOR_MEMORY_EXTENSION_TEST_H
#define MEMORY_ALLOCATOR_MEMORY_EXTENSION_TEST_H

#endif //MEMORY_ALLOCATOR_MEMORY_EXTENSION_TEST_H
#include "../mem.h"
#include "../mem_internals.h"
#include "../util.h"

bool memory_extension_test(void* heap);

void fourth_test_result_writer(bool result);

