CFLAGS=--std=c17 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG
BUILDDIR=build
SRCDIR=src
CC=gcc

all: $(BUILDDIR)/mem.o $(BUILDDIR)/util.o $(BUILDDIR)/mem_debug.o $(BUILDDIR)/main.o $(BUILDDIR)/free_one_block_test.o $(BUILDDIR)/free_two_blocks_test.o $(BUILDDIR)/memory_allocation_test.o $(BUILDDIR)/memory_extension_test.o $(BUILDDIR)/memory_extension_separated_test.o
	$(CC) -o $(BUILDDIR)/main $^

build:
	mkdir -p $(BUILDDIR)

$(BUILDDIR)/mem.o: $(SRCDIR)/mem.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/mem_debug.o: $(SRCDIR)/mem_debug.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/util.o: $(SRCDIR)/util.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/main.o: $(SRCDIR)/main.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/free_one_block_test.o: $(SRCDIR)/tests/free_one_block_test.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/free_two_blocks_test.o: $(SRCDIR)/tests/free_two_blocks_test.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/memory_allocation_test.o: $(SRCDIR)/tests/memory_allocation_test.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/memory_extension_test.o: $(SRCDIR)/tests/memory_extension_test.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/memory_extension_separated_test.o: $(SRCDIR)/tests/memory_extension_separated_test.c build
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -rf $(BUILDDIR)

